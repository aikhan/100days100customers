//
//  more_table.m
//  Gaint_heap
//
//  Created by Kim Klaver on 15/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "more_table.h"
#import "GiantheapAppDelegate.h"

#define DARK_BACKGROUND  [UIColor colorWithRed:151.0/255.0 green:152.0/255.0 blue:155.0/255.0 alpha:1.0]

@implementation more_table

@synthesize tabel, Cell, cellBG, namVal2, namVal1, next1, next;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabel.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBarHidden = YES;

}



//Implementing the table view to show the data on the more screen

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    return [appdelegate.icons count];
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	
	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	Cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
	
	//AppDelegate_iPhone *appdelegate= (AppDelegate_iPhone *)[[UIApplication sharedApplication]delegate];
	if (Cell==nil) 
	{
		CGRect cellFrame=CGRectMake(0,0, 300, 200);
		Cell=[[UITableViewCell alloc] initWithFrame:cellFrame reuseIdentifier:cellIdentifier];
	}
	
	CGRect next2=CGRectMake(295, 15, 20, 20);
	next=[[UIImageView alloc]initWithFrame:next2];
	UIImage *logo=[UIImage imageNamed:@"rightArrow.png"];
	next.image=logo;
	next.backgroundColor = [UIColor clearColor];
	[Cell.contentView addSubview:next];
	
	
	CGRect next3=CGRectMake(0, 5, 50, 50);
	next1=[[UIImageView alloc]initWithFrame:next3];
	UIImage *logo1=[UIImage imageNamed:[appdelegate.icons objectAtIndex:indexPath.row]];
	next1.image=logo1;
	[Cell.contentView addSubview:next1];
	
	
	CGRect nameValue1=CGRectMake(55,20,250 ,30); 
	namVal1=[[UILabel alloc] initWithFrame:nameValue1];	
	[Cell.contentView addSubview:namVal1];
	//namVal.text=[Links objectAtIndex:indexPath.row];
	namVal1.text=[appdelegate.appdes objectAtIndex:indexPath.row];
	namVal1.font=[UIFont systemFontOfSize:12];
	//namVal1.font = [UIFont boldSystemFontOfSize:14];
	namVal1.numberOfLines=2;
	
	namVal1.backgroundColor = [UIColor clearColor];

	//namVal1.font=[UIFont b
	
	CGRect nameValue2=CGRectMake(55,0,180 ,20); 
	namVal2=[[UILabel alloc] initWithFrame:nameValue2];	
	[Cell.contentView addSubview:namVal2];
	//namVal.text=[Links objectAtIndex:indexPath.row];
	namVal2.text=[appdelegate.iphoneapp objectAtIndex:indexPath.row];
	namVal2.font=[UIFont boldSystemFontOfSize:16];
	
	namVal2.backgroundColor = [UIColor clearColor];
    
    
    cellBG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 41)];
    cellBG.image = [UIImage imageNamed:@"tableBG.png"];
    
	Cell.textLabel.backgroundColor = [UIColor clearColor];
    Cell.textLabel.textColor = [UIColor blackColor];
    [Cell setBackgroundView:cellBG];
    
	
	return Cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.tabel deselectRowAtIndexPath:indexPath animated:YES];

	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[appdelegate.more_links objectAtIndex:indexPath.row]]];
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
