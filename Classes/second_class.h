//
//  second_class.h
//  Gaint_heap
//
//  Created by Kim Klaver on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface second_class : UIViewController<UITableViewDataSource , UITableViewDelegate> {
	IBOutlet UITableView *table;
    UITableViewCell *Cell;
    
    UIImageView * cellBG;
    UIImageView *next1;
    UILabel *namVal2;
    UIImageView  *next;
}

@property (nonatomic,retain)UITableView *table;
@property (nonatomic,retain) UITableViewCell *Cell;
@property (nonatomic,retain) UIImageView * cellBG;
@property (nonatomic,retain) UIImageView *next1;
@property (nonatomic,retain) UILabel *namVal2;
@property (nonatomic,retain) UIImageView  *next;
@end
