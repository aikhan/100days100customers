//
//  first_class.m
//  Gaint_heap
//
//  Created by Kim Klaver on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "first_class.h"
#import <AVFoundation/AVFoundation.h>
#import "GiantheapAppDelegate.h"


@implementation first_class
@synthesize table,volumeControl,audioControlBG,index,audioPlayerPaused,logo,cd, progressTimer, soundData, url, filePath, documentPath, tab, cell, cellBG;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
//	[table reloadData];
    GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if([appdelegate.srt isEqualToString:@"1"])
	{
		self.logo.image = [UIImage imageNamed:@"picon.png"];
        self.cd.text = @"1";
        maxCount = [appdelegate.cd1 count];
	}
	if([appdelegate.srt isEqualToString:@"2"])
	{
		self.logo.image = [UIImage imageNamed:@"create.png"];
        self.cd.text = @"2";
         maxCount = [appdelegate.cd2 count];
	}
    if([appdelegate.srt isEqualToString:@"3"])
	{
		self.logo.image = [UIImage imageNamed:@"yourhotbutton.png"];
        self.cd.text = @"3";
         maxCount = [appdelegate.cd3 count];
	}
    if([appdelegate.srt isEqualToString:@"4"])
	{
		self.logo.image = [UIImage imageNamed:@"find.png"];
        self.cd.text = @"4";
         maxCount = [appdelegate.cd4 count];
	}
    if([appdelegate.srt isEqualToString:@"5"])
	{
		self.logo.image = [UIImage imageNamed:@"advisor.png"];
        self.cd.text = @"5";
         maxCount = [appdelegate.cd5 count];
	}

	self.navigationController.navigationBarHidden = YES;
    
    [self.volumeControl setMaximumValue:1.0];
    [self.volumeControl setMinimumValue:0.0];
    
    [[AVAudioSession sharedInstance] setDelegate: self];  
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [UIApplication sharedApplication].idleTimerDisabled=YES;
    
    selecteAudio = -1;
	
}

-(IBAction) backButtonPressed
{
    [audioPlayer stop];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) previous
{
    [audioPlayer stop];
    GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    selecteAudio --;
    self.index --;
    if (self.index < 0) {
        self.index = 0;
        selecteAudio = 0;
    }
    if([appdelegate.srt isEqualToString:@"1"])
	{
		[self test:[appdelegate.cd1 objectAtIndex:self.index]];
	}
	if([appdelegate.srt isEqualToString:@"2"])
	{
		[self test:[appdelegate.cd2 objectAtIndex:self.index]];
	}
    if([appdelegate.srt isEqualToString:@"3"])
	{
		[self test:[appdelegate.cd3 objectAtIndex:self.index]];
	}
    if([appdelegate.srt isEqualToString:@"4"])
	{
		[self test:[appdelegate.cd4 objectAtIndex:self.index]];
	}
    if([appdelegate.srt isEqualToString:@"5"])
	{
		[self test:[appdelegate.cd5 objectAtIndex:self.index]];
	}
    
    [table reloadData];
}
-(IBAction) pauseOrplay
{
    GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    if(audioPlayer)
    {
        if (self.audioPlayerPaused) {
            self.audioControlBG.image = [UIImage imageNamed:@"audioControlPause.png"];
            
            if (isPlayerFinished == YES) {
                if([appdelegate.srt isEqualToString:@"1"])
                {
                    [self test:[appdelegate.cd1 objectAtIndex:self.index]];
                }
                if([appdelegate.srt isEqualToString:@"2"])
                {
                    [self test:[appdelegate.cd2 objectAtIndex:self.index]];
                }
                if([appdelegate.srt isEqualToString:@"3"])
                {
                    [self test:[appdelegate.cd3 objectAtIndex:self.index]];
                }
                if([appdelegate.srt isEqualToString:@"4"])
                {
                    [self test:[appdelegate.cd4 objectAtIndex:self.index]];
                }
                if([appdelegate.srt isEqualToString:@"5"])
                {
                    [self test:[appdelegate.cd5 objectAtIndex:self.index]];
                }
                
                isPlayerFinished = NO;
                
            }
            else {
                [audioPlayer play];
            }
//            
            self.audioPlayerPaused = NO;
        }
        else
        {
            self.audioControlBG.image = [UIImage imageNamed:@"audioControl.png"];
            [audioPlayer pause];
            self.audioPlayerPaused = YES;
        }
    }
    else
    {
        
        if([appdelegate.srt isEqualToString:@"1"])
        {
            [self test:[appdelegate.cd1 objectAtIndex:0]];
        }
        if([appdelegate.srt isEqualToString:@"2"])
        {
            [self test:[appdelegate.cd2 objectAtIndex:0]];
        }
        if([appdelegate.srt isEqualToString:@"3"])
        {
            [self test:[appdelegate.cd3 objectAtIndex:0]];
        }
        if([appdelegate.srt isEqualToString:@"4"])
        {
            [self test:[appdelegate.cd4 objectAtIndex:0]];
        }
        if([appdelegate.srt isEqualToString:@"5"])
        {
            [self test:[appdelegate.cd5 objectAtIndex:0]];
        }
        
        selecteAudio = 0;
        [table reloadData];
    }
    
    
    
}
-(IBAction) next
{
    [audioPlayer stop];
    GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.index ++;

    selecteAudio = selecteAudio + 1;
    
    if([appdelegate.srt isEqualToString:@"1"])
	{
        if (self.index == [appdelegate.cd1 count]) {
            self.index = [appdelegate.cd1 count] - 1;
        }
        if (selecteAudio > 14) {
            selecteAudio = 14;
        }
		[self test:[appdelegate.cd1 objectAtIndex:self.index]];
	}
	if([appdelegate.srt isEqualToString:@"2"])
    {
        if (self.index == [appdelegate.cd2 count]) {
            self.index = [appdelegate.cd2 count] - 1;
        }
        if (selecteAudio > 8) {
            selecteAudio = 8;
        }
		[self test:[appdelegate.cd2 objectAtIndex:self.index]];
    }
    if([appdelegate.srt isEqualToString:@"3"])
    {
        if (self.index == [appdelegate.cd3 count]) {
            self.index = [appdelegate.cd3 count] - 1;
        }
        if (selecteAudio > 13) {
            selecteAudio = 13;
        }
		[self test:[appdelegate.cd3 objectAtIndex:self.index]];
    }
    if([appdelegate.srt isEqualToString:@"4"])
    {
        if (self.index == [appdelegate.cd4 count]) {
            self.index = [appdelegate.cd4 count] - 1;
        }
        if (selecteAudio > 13) {
            selecteAudio = 13;
        }
		[self test:[appdelegate.cd4 objectAtIndex:self.index]];
    }
    if([appdelegate.srt isEqualToString:@"5"])
    {
        if (self.index == [appdelegate.cd5 count]) {
            self.index = [appdelegate.cd5 count] - 1;
        }
        if (selecteAudio > 5) {
            selecteAudio = 5;
        }
		[self test:[appdelegate.cd5 objectAtIndex:self.index]];
    }
    
    [table reloadData];
}

-(IBAction) volumeChanged
{
    audioPlayer.volume = self.volumeControl.value;
}

#pragma mark -
#pragma mark *play*
- (void)test:(NSString *)input {
    
    GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSLog(@"input %@", input);
    documentPath = input;
    [documentPath retain];
    
    
    if ([appdelegate.srt isEqualToString:@"3"] || [appdelegate.srt isEqualToString:@"4"] || [appdelegate.srt isEqualToString:@"5"]) {
        NSString * inputString = [NSString stringWithFormat:@"%@.mp3", input];
        filePath = [[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                   NSUserDomainMask, YES) objectAtIndex:0] 
                              stringByAppendingPathComponent:inputString] retain];
        NSLog(@"file path 5: %@", filePath);
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL
                                                                        fileURLWithPath:filePath] error:NULL];
        }
        else {
            
//            [self progressViewLoader];
            [overlayView setHidden:NO];
            
            NSString *audioString = [NSString stringWithFormat:@"http://semanticdevlab.com/100days/one/%@", inputString];
            audioString = [audioString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            NSLog(@"audio string %@", audioString);
            [self progressViewLoader:audioString];
            isDownloading = YES;
//            url = [NSURL URLWithString:audioString]; 
//            NSLog(@"url path %@", url);
//            [self performSelectorInBackground:@selector(downloadAudio) withObject:nil];
////            soundData = [NSData dataWithContentsOfURL:url];
//            NSLog(@"sound data : %@", soundData);
//            NSLog(@"file path %@", filePath);
//            [soundData writeToFile:filePath atomically:YES];
//            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL
//                                                                        fileURLWithPath:filePath] error:NULL];  
            NSLog(@"downloading");
        }
        

    }
    else
    {
        NSString *newAudioFile = [[NSBundle mainBundle] pathForResource:input ofType:@"mp3"];
        
        NSURL *fileURL = [[[NSURL alloc] initFileURLWithPath:newAudioFile]autorelease];
        
        audioPlayer =	[[AVAudioPlayer alloc] initWithContentsOfURL: fileURL error: nil];
    }
    
    NSLog(@" test ");
    
    if (!isDownloading) {
        self.audioControlBG.image = [UIImage imageNamed:@"audioControlPause.png"];
        self.audioPlayerPaused = NO;
        
        progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        // Set the maximum value of the UISlider
        progressControl.maximumValue = audioPlayer.duration;
        // Set the valueChanged target
        //    [progressControl addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
        
        NSLog(@"prepare to play audio");
        [audioPlayer setDelegate:self]; 
        [audioPlayer prepareToPlay]; 
        [audioPlayer play]; 
        audioPlayer.volume = 0.5;
        titleLabel.text = input;
    }
	
    
}

- (void)updateSlider {
    // Update the slider about the music time
    progressControl.value = audioPlayer.currentTime;
    int min = audioPlayer.currentTime/60;
    int sec = audioPlayer.currentTime;
    sec = sec % 60;
    NSString *timeString = [NSString stringWithFormat:@"%d:%d", min, sec];
    timeLabel.text = timeString;
}

- (IBAction)sliderChanged:(UISlider *)sender {
    // Fast skip the music when user scroll the UISlider
    [audioPlayer stop];
    [audioPlayer setCurrentTime:progressControl.value];
    [audioPlayer prepareToPlay];
    if (audioControlBG.image == [UIImage imageNamed:@"audioControlPause.png"]) {
        [audioPlayer play];
    }
    
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    // Music completed
    if (flag) {
        [progressTimer invalidate];
        NSLog(@"audio player did finish playing");
        progressControl.value = 0;
        self.audioControlBG.image = [UIImage imageNamed:@"audioControl.png"];
        self.audioPlayerPaused = YES;
        isPlayerFinished = YES;
        if (index < maxCount - 1) {
            [self next];
        }
        
    }
}

-(void)downloadAudio
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    soundData = [NSData dataWithContentsOfURL:url];
    [overlayView setHidden:YES];
    [pool release];
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    int rows;
    rows = 0;
    
	if([appdelegate.srt isEqualToString:@"1"])
    {
        rows = [appdelegate.cd1 count];
    }
	if([appdelegate.srt isEqualToString:@"2"])
    {
        rows = [appdelegate.cd2 count];
    }
    if([appdelegate.srt isEqualToString:@"3"])
    {
        rows = [appdelegate.cd3 count];
    }
    if([appdelegate.srt isEqualToString:@"4"])
    {
        rows = [appdelegate.cd4 count];
    }
    if([appdelegate.srt isEqualToString:@"5"])
    {
        rows = [appdelegate.cd5 count];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	
	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
		
	//AppDelegate_iPhone *appdelegate= (AppDelegate_iPhone *)[[UIApplication sharedApplication]delegate];
	if([appdelegate.srt isEqualToString:@"1"])
	{
		cell.textLabel.text = [appdelegate.cd1 objectAtIndex:indexPath.row];
	}
	if([appdelegate.srt isEqualToString:@"2"])
	{
		cell.textLabel.text = [appdelegate.cd2 objectAtIndex:indexPath.row];
	}
    if([appdelegate.srt isEqualToString:@"3"])
	{
		cell.textLabel.text = [appdelegate.cd3 objectAtIndex:indexPath.row];
	}
    if([appdelegate.srt isEqualToString:@"4"])
	{
		cell.textLabel.text = [appdelegate.cd4 objectAtIndex:indexPath.row];
	}
    if([appdelegate.srt isEqualToString:@"5"])
	{
		cell.textLabel.text = [appdelegate.cd5 objectAtIndex:indexPath.row];
	}
	
    cellBG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 41)];
    cellBG.image = [UIImage imageNamed:@"table.png"];
    
	cell.textLabel.backgroundColor = [UIColor clearColor];
    if (selecteAudio == indexPath.row) {
        cell.textLabel.textColor = [UIColor colorWithRed:.64 green:.22 blue:.02 alpha:1.0];
    }
    else 
    {
        cell.textLabel.textColor = [UIColor blackColor];
    }
    [cell setBackgroundView:cellBG];

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	[audioPlayer stop];
    
    
    self.index = indexPath.row;
	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
	if([appdelegate.srt isEqualToString:@"1"])
	{
		[self test:[appdelegate.cd1 objectAtIndex:indexPath.row]];
	}
	if([appdelegate.srt isEqualToString:@"2"])
	{
		[self test:[appdelegate.cd2 objectAtIndex:indexPath.row]];
	}
    if([appdelegate.srt isEqualToString:@"3"])
	{
		[self test:[appdelegate.cd3 objectAtIndex:indexPath.row]];
	}
    if([appdelegate.srt isEqualToString:@"4"])
	{
		[self test:[appdelegate.cd4 objectAtIndex:indexPath.row]];
	}
    if([appdelegate.srt isEqualToString:@"5"])
	{
		[self test:[appdelegate.cd5 objectAtIndex:indexPath.row]];
	}
    
    
	[self.table deselectRowAtIndexPath:indexPath animated:YES];
    
    selecteAudio = indexPath.row;
    [table reloadData];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
  */

#pragma mark - Loader Veiw

- (void)progressViewLoader:(NSString*)link{
	
	progressLabel.text = @"0\%";

	soundData = [[NSMutableData alloc]init];
	NSURL *urlString = [NSURL URLWithString:link];
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:urlString];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	[connection start];
	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{

    [overlayView setHidden:NO];
	totalfilesize = [response expectedContentLength];
	[soundData setLength:0];
    NSLog(@"conection did recieve response");
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
//	[overlayView setHidden:NO];
	filesizereceived += [data length];
	filepercentage = (float)filesizereceived/(float)totalfilesize;
	progressView.progress = filepercentage;
	
	int p = 100 *filepercentage;
    if (p > 100) {
        p = 100;
    }
	NSString *percent = [NSString stringWithFormat: @"%d %%",p];
	
	progressLabel.text = percent;
	[soundData appendData:data];
	NSLog(@"conection did recieve data");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    isDownloading = NO;
    NSLog(@"conection did finish");
	[overlayView setHidden:YES];
//    NSLog(@"sound data : %@", soundData);
    NSLog(@"file path 2: %@", filePath);
    if (soundData == nil) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"Try Later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else 
    {
        [soundData writeToFile:filePath atomically:YES];
        [self test:documentPath];
    }
    
//    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL
//                                                                fileURLWithPath:filePath] error:NULL];
//    [audioPlayer play];
	[connection release];
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (void)dealloc {
    
    [super dealloc];
    [audioPlayer release];
}


@end
