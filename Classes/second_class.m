//
//  second_class.m
//  Gaint_heap
//
//  Created by Kim Klaver on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "second_class.h"
#import "GiantheapAppDelegate.h"

@implementation second_class
@synthesize table, Cell, cellBG, namVal2, next1, next;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.table.backgroundColor = [UIColor clearColor]; 
    
    self.navigationController.navigationBarHidden = YES;
}



//Code for the table view to display the data

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
	return 3;
	
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	
	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	Cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
	
	//AppDelegate_iPhone *appdelegate= (AppDelegate_iPhone *)[[UIApplication sharedApplication]delegate];
	if (Cell==nil) 
	{
		CGRect cellFrame=CGRectMake(0,0, 300, 200);
		Cell=[[UITableViewCell alloc] initWithFrame:cellFrame reuseIdentifier:cellIdentifier];	
    }
    
    tableView.separatorColor = [UIColor clearColor];
	
	CGRect next2=CGRectMake(295, 15, 20, 20);
	next=[[UIImageView alloc]initWithFrame:next2];
	UIImage *logo=[UIImage imageNamed:@"rightArrow.png"];
	next.image=logo;
	next.backgroundColor = [UIColor clearColor];
	[Cell.contentView addSubview:next];
	
	
//	CGRect next3=CGRectMake(0, 5, 50, 50);
	next1=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 50, 50)];
	UIImage *logo1=[UIImage imageNamed:[appdelegate.upcoming_img objectAtIndex:indexPath.row]];
	next1.image=logo1;
	[Cell.contentView addSubview:next1];
	
	
//	CGRect nameValue2=CGRectMake(55,5,320 ,20); 
	namVal2=[[UILabel alloc] initWithFrame:CGRectMake(55, 5, 320, 20)];	
	[Cell.contentView addSubview:namVal2];
	//namVal.text=[Links objectAtIndex:indexPath.row];
	namVal2.text=[appdelegate.upcoming_arr objectAtIndex:indexPath.row];
	namVal2.font=[UIFont boldSystemFontOfSize:17];
	
	namVal2.backgroundColor = [UIColor clearColor];
    
    cellBG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 41)];
    cellBG.image = [UIImage imageNamed:@"tableBG.png"];
    
	Cell.textLabel.backgroundColor = [UIColor clearColor];
    Cell.textLabel.textColor = [UIColor blackColor];
    [Cell setBackgroundView:cellBG];
	
	return Cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/one-liners/id470603847?mt=8"]];
    }
    else if (indexPath.row == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/giant-heap/id468376276?mt=8"]];
    }
    else 
    {
        [self.table deselectRowAtIndexPath:indexPath animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Coming Apps" message:@"Coming to iTunes next week" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
