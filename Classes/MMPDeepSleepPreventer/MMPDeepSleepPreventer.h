

#import <Foundation/Foundation.h>

@class AVAudioPlayer;


@interface MMPDeepSleepPreventer : NSObject
{
 @private
	AVAudioPlayer	*audioPlayer_;
	NSTimer			*preventSleepTimer_;
    
}

@property (nonatomic, retain) AVAudioPlayer	*audioPlayer;
@property (nonatomic, retain) NSTimer		*preventSleepTimer;


- (void)startPreventSleep;
- (void)stopPreventSleep;

@end
