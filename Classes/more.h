//
//  more.h
//  Gaint_heap
//
//  Created by Kim Klaver on 9/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface more : UIViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate> {
	IBOutlet UIWebView *web;
	IBOutlet UIButton *custom_but1;
	IBOutlet UIButton *custom_but2;
	IBOutlet UIButton *custom_but3;
}

-(IBAction)but1;
-(IBAction)but2;
-(IBAction)but3;

@end
