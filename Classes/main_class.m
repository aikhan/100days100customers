//
//  main_class.m
//  Gaint_heap
//
//  Created by Kim Klaver on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "main_class.h"
#import "first_class.h"
#import "second_class.h"
#import "GiantheapAppDelegate.h"

@implementation main_class

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
	
}


//Function to call the new class to play the gaint heap cd1
-(IBAction)cd1_pressed
{
	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
	appdelegate.srt = @"1";
									  
	first_class *FVC = [[first_class alloc ] initWithNibName:@"first_class" bundle:nil];
	[self.navigationController pushViewController:FVC animated:YES];
    [FVC release];
	
}

//Function to call the class to play the gaint heap cd2
-(IBAction)cd2_pressed
{
	GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
	appdelegate.srt = @"2";
	first_class *FVC = [[first_class alloc ] initWithNibName:@"first_class" bundle:nil];
	[self.navigationController pushViewController:FVC animated:YES];
    [FVC release];

}

-(IBAction)cdButtonTapped:(id)sender
{
    GiantheapAppDelegate *appdelegate= (GiantheapAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 1) {
        appdelegate.srt = @"1";
    }
    if (btn.tag == 2) {
        appdelegate.srt = @"2";
    }
    if (btn.tag == 3) {
        appdelegate.srt = @"3";
    }
    if (btn.tag == 4) {
        appdelegate.srt = @"4";
    }
    if (btn.tag == 5) {
        appdelegate.srt = @"5";
    }
    
    first_class *FVC = [[first_class alloc ] initWithNibName:@"first_class" bundle:nil];
	[self.navigationController pushViewController:FVC animated:YES];
    [FVC release];
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
