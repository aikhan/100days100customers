//
//  more.m
//  Gaint_heap
//
//  Created by Kim Klaver on 9/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "more.h"


@implementation more

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
}

//code for calling the website
-(IBAction)but1
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bananamarketing.com/"]];

}

//Kim's blog
//http://kimklaverblogs.com
//
//Kim Klaver
//kimklaver@mac.com

//code to call the blog
-(IBAction)but2
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://kimklaverblogs.com"]];

}

//code to send the mail to the user
-(IBAction)but3
{
	if([MFMailComposeViewController canSendMail])
	{
		MFMailComposeViewController *mailer =[[MFMailComposeViewController alloc]init];
		mailer.mailComposeDelegate=self;
		
			[mailer setToRecipients:[NSArray arrayWithObject:@"kimklaver@mac.com"]];
			[mailer setMessageBody:@"" isHTML:NO];
			[mailer setSubject:@""];
			[self presentModalViewController:mailer animated:YES];
	}
	else 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Mail is not configured" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
		[alert show];
		[alert release];
	}
	
}	

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	if (result == MFMailComposeResultSent)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sent" message:@"Your mail is sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
		[alert show];
		[alert release];
	}
	
	else if (result == MFMailComposeResultFailed)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
		[alert show];
		[alert release];
	}
	
	else if (result == MFMailComposeResultSaved)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved" message:@"Your mail is saved to draft" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
		[alert show];
		[alert release];
	}
	
	[self dismissModalViewControllerAnimated:YES];
	
	//table1.hidden = YES;
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
