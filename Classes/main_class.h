//
//  main_class.h
//  Gaint_heap
//
//  Created by Kim Klaver on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface main_class : UIViewController {
	IBOutlet UIButton *cd1;
	IBOutlet UIButton *cd2;
}

-(IBAction)cd1_pressed;
-(IBAction)cd2_pressed;
-(IBAction)cdButtonTapped:(id)sender;


@end
