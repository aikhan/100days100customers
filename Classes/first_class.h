//
//  first_class.h
//  Gaint_heap
//
//  Created by Kim Klaver on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "tab_bar.h"
@class tab_bar;

@interface first_class : UIViewController<AVAudioPlayerDelegate,UITableViewDataSource,UITableViewDelegate> {
	AVAudioPlayer *audioPlayer;
	IBOutlet UIImageView *img;
	IBOutlet UITableView *table;
    IBOutlet UISlider * volumeControl;
    IBOutlet UIImageView * audioControlBG;
    IBOutlet UIImageView * logo;
    IBOutlet UILabel * cd;
    
    IBOutlet UILabel *timeLabel;
    IBOutlet UILabel *titleLabel;
    
    IBOutlet UISlider *progressControl;
    NSTimer *progressTimer;
    int selecteAudio;
    
    NSInteger index;
    BOOL audioPlayerPaused;
    BOOL isPlayerFinished;
    BOOL isDownloading;
    
    IBOutlet UIView *overlayView;
    
    NSMutableData *soundData;
    NSURL *url;
    
    int maxCount;
    
    IBOutlet UIProgressView *progressView;
	IBOutlet UILabel *progressLabel;
	int totalfilesize;
	int filesizereceived;
	float filepercentage;
    
    NSString *filePath;
    NSString *documentPath;
    
    tab_bar *tab;
    UITableViewCell *cell;
    UIImageView * cellBG;
  
    
}
@property (nonatomic, retain)UITableView *table;
@property (nonatomic, retain)IBOutlet UISlider * volumeControl;
@property (nonatomic, retain)IBOutlet UIImageView * audioControlBG;
@property (nonatomic,readwrite) NSInteger index;
@property (nonatomic,readwrite) BOOL audioPlayerPaused;
@property (nonatomic,retain) IBOutlet UIImageView * logo;
@property (nonatomic,retain) IBOutlet UILabel * cd;
@property (nonatomic,retain) NSTimer *progressTimer;
@property (nonatomic,retain) NSMutableData *soundData;
@property (nonatomic,retain) NSURL *url;
@property (nonatomic,retain) NSString *filePath;
@property (nonatomic,retain) NSString *documentPath;
@property (nonatomic,retain) tab_bar *tab;
@property (nonatomic,retain) UITableViewCell *cell;
@property (nonatomic,retain) UIImageView * cellBG;

- (void)test:(NSString *)input;
-(IBAction) backButtonPressed;

-(IBAction) previous;
-(IBAction) pauseOrplay;
-(IBAction) next;
-(IBAction) volumeChanged;
-(void)downloadAudio;
- (void)progressViewLoader:(NSString*)link;


@end
