//
//  tab_bar.m
//  Gaint_heap
//
//  Created by Kim Klaver on 13/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "tab_bar.h"


@implementation tab_bar

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self setUpTabBar];
	[self.view setFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
	[self.view addSubview:settab.view];
}

- (void) setUpTabBar
{
	settab = [[UITabBarController alloc] init];
	settab.delegate = self;
	self.navigationController.navigationBarHidden = TRUE;
	
	tab1 = [[main_class alloc] initWithNibName:@"main_class" bundle:[NSBundle mainBundle]];
	UINavigationController *ncvm = [[[UINavigationController alloc] initWithRootViewController:tab1] autorelease];
	ncvm.title = @"100 Customers 100 Days";
	//[ncvm setNavigationBarHidden:YES];
	tab1.title = @"100 Customers";
	tab1.tabBarItem.image = [UIImage imageNamed:@"giantheap.png"];
	[tab1 release];
	
	tab3 = [[second_class alloc] initWithNibName:@"second_class" bundle:[NSBundle mainBundle]];
	UINavigationController *ncnm = [[[UINavigationController alloc] initWithRootViewController:tab3] autorelease];
	ncnm.title =@"PDF";
	ncnm.title=@"Face";
	//[ncnm setNavigationBarHidden:YES];
	
	tab3.title = @"Kim Apps";
	
	tab3.tabBarItem.image = [UIImage imageNamed:@"comingapps.png"];
	[tab3 release];
			
	tab12 = [[more_table alloc] initWithNibName:@"more_table" bundle:[NSBundle mainBundle]];
	UINavigationController *ncvm1 = [[[UINavigationController alloc] initWithRootViewController:tab12] autorelease];
	ncvm1.title = @"More";
 	//[ncvm1 setNavigationBarHidden:YES];
	tab12.title = @"More";
	tab12.tabBarItem.image = [UIImage imageNamed:@"more.png"];
	[tab12 release];
	
	tab4 = [[more alloc] initWithNibName:@"more" bundle:[NSBundle mainBundle]];
	UINavigationController *ncvm2 = [[[UINavigationController alloc] initWithRootViewController:tab4] autorelease];
	ncvm2.title = @"Contact Us";
//	[ncvm2 setNavigationBarHidden:YES];
	tab4.title = @"Contact Us";
	tab4.tabBarItem.image = [UIImage imageNamed:@"contactus.png"];
	[tab4 release];
	
	[settab.view addSubview:but];
	settab.viewControllers = [NSArray arrayWithObjects:ncvm,ncnm,ncvm2,ncvm1, nil];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
