//
//  GiantheapAppDelegate.h
//  Giantheap
//
//  Created by Kim Klaver on 17/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPDeepSleepPreventer.h"
@class GiantheapViewController;

@interface GiantheapAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    GiantheapViewController *viewController;
	NSTimer *timer; 
	UIImageView *imageview;
	UINavigationController *nav;
	NSMutableArray *cd1;
	NSMutableArray *cd2;
    NSMutableArray *cd3;
    NSMutableArray *cd4;
    NSMutableArray *cd5;
    
	NSString *srt;
	NSMutableArray *iphoneapp;
	NSMutableArray *appdes;
	NSMutableArray *icons;
	NSMutableArray *upcoming_arr;
	NSMutableArray *upcoming_img;
	NSMutableArray *more_links;
    MMPDeepSleepPreventer *deepSleepPreventer;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet GiantheapViewController *viewController;
@property (nonatomic, retain) NSMutableArray *cd1;
@property (nonatomic, retain) NSMutableArray *cd2;
@property (nonatomic, retain) NSMutableArray *cd3;
@property (nonatomic, retain) NSMutableArray *cd4;
@property (nonatomic, retain) NSMutableArray *cd5;
@property (nonatomic, retain) NSMutableArray *iphoneapp;
@property (nonatomic, retain) NSMutableArray *appdes;
@property (nonatomic, retain) NSMutableArray *icons;
@property (nonatomic, retain) NSMutableArray *upcoming_arr;
@property (nonatomic, retain) NSMutableArray *upcoming_img;
@property (nonatomic, retain) NSString *srt;
@property (nonatomic, retain) NSMutableArray *more_links;
@property (nonatomic, retain) MMPDeepSleepPreventer *deepSleepPreventer;

@end

