//
//  more_table.h
//  Gaint_heap
//
//  Created by Kim Klaver on 15/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface more_table : UIViewController<UITableViewDelegate,UITableViewDataSource> {
	
	IBOutlet UITableView *tabel;
    UITableViewCell *Cell;
    UIImageView  *next;
    UIImageView *next1;
    UILabel *namVal1;
    UILabel *namVal2;
    UIImageView * cellBG;
	
}

@property (nonatomic, retain) UITableView *tabel;
@property (nonatomic, retain) UITableViewCell *Cell;
@property (nonatomic, retain) UIImageView  *next;
@property (nonatomic, retain) UIImageView *next1;
@property (nonatomic, retain) UILabel *namVal1;
@property (nonatomic, retain) UILabel *namVal2;
@property (nonatomic, retain) UIImageView * cellBG;
@end
