//
//  tab_bar.h
//  Gaint_heap
//
//  Created by Kim Klaver on 13/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "first_class.h"
#import "main_class.h"
#import "second_class.h"
#import "more.h"
#import "iphone_exp.h"
#import "more_table.h"
@class first_class;

@interface tab_bar : UIViewController<UITabBarControllerDelegate> {
	UITabBarController *settab;
	first_class *tab1;
	main_class *tab2;
	second_class *tab3;
	more *tab4;
	more_table *tab12;
	IBOutlet UIButton *but; 
}

- (void) setUpTabBar;
@end
