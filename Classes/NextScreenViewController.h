//
//  NextScreenViewController.h
//  Giantheap
//
//  Created by Asad Khan on 11/3/11.
//  Copyright (c) 2011 Semantic Notion Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NextScreenViewController : UIViewController
-(IBAction)nextTapped:(id)sender;
@end
