//
//  GiantheapAppDelegate.m
//  Giantheap
//
//  Created by Kim Klaver on 17/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GiantheapAppDelegate.h"
#import "GiantheapViewController.h"
#import "tab_bar.h"
//#import "FlurryAnalytics.h"
#import "NextScreenViewController.h"
#import "Flurry.h"

@class MMPDeepSleepPreventer;

@implementation GiantheapAppDelegate

@synthesize window;
@synthesize viewController;
@synthesize cd1,cd2,cd3,cd4,cd5,srt,iphoneapp,appdes,icons,upcoming_arr,upcoming_img,more_links, deepSleepPreventer;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [Flurry startSession:@"DDW8EZKMRZ5IGIKI9JLP"];
    
    [UIApplication sharedApplication].idleTimerDisabled=YES;
    self.deepSleepPreventer = [[[MMPDeepSleepPreventer alloc] init] autorelease];
	[self.deepSleepPreventer startPreventSleep];

	// Set the view controller as the window's root view controller and display.
	cd1 = [[NSMutableArray alloc] initWithObjects:@"1. Would You Use It?",@"2. Insurance Income",@"3. Did You Have A Clue?",@"4. What's Wrong With 9 In 10?",@"5. Therapy?",@"6. Venters Or Changers?",@"7. The First Agreement",@"8. Say 'No' First",@"9. How Not To Lose Them",@"10. In The Customer's Shoes",@"11. Seller-Talk Signs",@"12. Generalities",@"13. Techo-Babble",@"14. Hype",@"15. Seller-Talk Examples",nil];
	
	cd2 = [[NSMutableArray alloc]initWithObjects:@"1. Insurance income",@"2. What To Say To The 1 In 10",@"3. Call Their Name",@"4. Your Hot Button",@"5. Before, I Was Someone Who...",@"6. Creating A First Date Script Live",@"7. Extra First Date Script",@"8. Finding Your Audience",@"9. Scripting Tips",nil];
    
    cd3 = [[NSMutableArray alloc] initWithObjects:@"1. Seller-talk signs",@"2. What If They Say Yes",@"3. What Is It",@"4. Will It Work For Me",@"5.  Info: To Send Or Not",@"6. How Much Is It",@"7. Surprise Advice For Customer",@"8. Membership And Autoship",@"9. Cable TV Man",@"10. 5 Worst Things To Say",@"11. Practice Smarter -- MD Story",@"12. 7x A Day 21 Days",@"13. Deadly Distractions",@"14. 3 Tips To Keep Them On Phone",nil];
    
    cd4 = [[NSMutableArray alloc] initWithObjects:@"1. Marketing Checklist",@"2. Warm Market",@"3. Old Customers",@"4. Referrals",@"5. Cold Market",@"6. Getting Past Gatekeeper",@"7. Door-to-Door",@"8. Organizing Your Prospects",@"9. Provide More Info",@"10. Message On Their Phone",@"11. Message On Your Phone",@"12. Is It Sharing",@"13. Business Tips",@"14. How To Do An Email Signature",nil];
    
    cd5 = [[NSMutableArray alloc] initWithObjects:@"1. Email Signatures",@"2. Larissa Script",@"3. Brook Script",@"4. Nicole Script",@"5. Two Old-New Script Matches",@"6. 7-Second First Date Script",nil];
	
	iphoneapp = [[NSMutableArray alloc] initWithObjects:@"Angry Birds", @"Scanner Pro",@"Scribblenauts Remix",@"Words with Friends",@"Splashtop Remote for iPod and iPhone",nil];
	
	appdes = [[NSMutableArray alloc] initWithObjects:@"The survival of the Angry Birds is at stake…", @"Scan multipage docs, email and upload to Dropbox…",@"Play Words with Friends…",@"Your PC or MAC in your pocket…",@"Scan multipage docs, email and upload to Dropbox…",nil];
	
	icons = [[NSMutableArray alloc] initWithObjects:@"birdy.jpg", @"proscanner.png",@"y2.png",@"y3.png",@"y4.png",nil];
	
	upcoming_arr= [[ NSMutableArray alloc] initWithObjects:@"One Liners",@"Giant Heap",@"Awesome Sponsor",nil];
	
	upcoming_img = [[ NSMutableArray alloc] initWithObjects:@"oneliner-icon.png",@"icon.png",@"sponsor.png",nil];
	
	more_links = [[NSMutableArray alloc]initWithObjects: @"http://click.linksynergy.com/fs-bin/stat?id=jsaAs1PJ0Ug&offerid=146261&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fitunes.apple.com%252Fus%252Fapp%252Fangry-birds%252Fid343200656%253Fmt%253D8%2526uo%253D4%2526partnerId%253D30",
                  @"http://click.linksynergy.com/fs-bin/stat?id=jsaAs1PJ0Ug&offerid=146261&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fitunes.apple.com%252Fus%252Fapp%252Fscanner-pro-scan-multipage%252Fid333710667%253Fmt%253D8%2526uo%253D4%2526partnerId%253D30",
                  @"http://click.linksynergy.com/fs-bin/stat?id=jsaAs1PJ0Ug&offerid=146261&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fitunes.apple.com%252Fus%252Fapp%252Fscribblenauts-remix%252Fid444844790%253Fmt%253D8%2526uo%253D4%2526partnerId%253D30",
                  @"http://click.linksynergy.com/fs-bin/stat?id=jsaAs1PJ0Ug&offerid=146261&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fitunes.apple.com%252Fus%252Fapp%252Fwords-with-friends%252Fid322852954%253Fmt%253D8%2526uo%253D4%2526partnerId%253D30",
                  @"http://click.linksynergy.com/fs-bin/stat?id=jsaAs1PJ0Ug&offerid=146261&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fitunes.apple.com%252Fus%252Fapp%252Fsplashtop-remote-desktop-for%252Fid402056323%253Fmt%253D8%2526uo%253D4%2526partnerId%253D30",nil];
	
	imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"100customers 100 days.png"]];
	
	imageview.frame=CGRectMake(0,0, 320, [UIScreen mainScreen].bounds.size.height);
	
	[window addSubview:imageview];
	
	tab_bar *lvc=[[tab_bar alloc]initWithNibName:@"tab_bar" bundle:nil];
	//	
	nav = [[UINavigationController alloc] initWithRootViewController:lvc];
	
    nav.view.alpha = 0.0;
	
	nav.navigationController.navigationBarHidden = TRUE ;
	
	timer=[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(fadeScreen) userInfo:nil repeats:NO];
	
	[window addSubview:nav.view];
	
	[self.window makeKeyAndVisible];
    NSString *soundFilePath =[[NSBundle mainBundle] pathForResource: @"KimAppIntroMusic" ofType: @"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    AVAudioPlayer *newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL: fileURL error: nil];
    [newPlayer play];
   
	
    return YES;
}

-(void)fadeScreen{
	imageview.alpha=0.5;
	[UIView beginAnimations:nil context:nil]; 
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(finishedFading) ];
	imageview.alpha=0.0;
	nav.view.alpha=1.0;
	[UIView commitAnimations];
	
}

-(void)finishedFading{
	
	[imageview removeFromSuperview];
    NextScreenViewController *nextVC = [[NextScreenViewController alloc] init];
    [nav presentModalViewController:nextVC animated:NO];
    [nextVC release];
	
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
