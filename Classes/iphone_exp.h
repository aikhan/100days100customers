//
//  iphone_exp.h
//  Gaint_heap
//
//  Created by Kim Klaver on 9/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface iphone_exp : UIViewController<UIWebViewDelegate> {
	IBOutlet UIWebView *web;
	IBOutlet UIActivityIndicatorView *actView;
	NSURL *iTunesURL;
}
@property(nonatomic, retain)NSURL *iTunesURL;

@end
